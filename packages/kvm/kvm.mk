KVM_URL=http://heanet.dl.sourceforge.net/sourceforge/kvm
KVM_VERSION=77
KVM_SOURCE=kvm-$(KVM_VERSION).tar.gz
KVM_DIR=$(BUILD_DIR)/kvm
KVM_SRC_DIR=$(KVM_DIR)/kvm-$(KVM_VERSION)
KVM_STAMP_DIR=$(KVM_DIR)/stamps

KVM_PATCHES=$(PACKAGE_DIR)/kvm/patches

CC_IPREFIX:=$(shell $(CC) --print-file-name=include)
LIMITS_CFLAGS = -I$(dir $(CC_IPREFIX))/include-fixed -I$(CC_IPREFIX)

$(SOURCE_DIR)/$(KVM_SOURCE):
	mkdir -p $(SOURCE_DIR)
	@ wget $(WGET_Q) -P $(SOURCE_DIR) $(KVM_URL)/$(KVM_SOURCE)

$(KVM_STAMP_DIR)/.unpacked: $(SOURCE_DIR)/$(KVM_SOURCE)
	tar -C $(KVM_DIR) -zxf $(SOURCE_DIR)/$(KVM_SOURCE)
	touch $@	

$(KVM_STAMP_DIR)/.patched: $(KVM_STAMP_DIR)/.unpacked
	@ echo "Patching kvm..."                           
	@ $(BIN_DIR)/doquilt.sh $(KVM_SRC_DIR) $(KVM_PATCHES)
	@ echo "all:" > $(KVM_SRC_DIR)/kernel/Makefile
	@ echo "all:" > $(KVM_SRC_DIR)/user/Makefile
	@ touch $@


$(KVM_STAMP_DIR)/.configured: $(KVM_STAMP_DIR)/.patched
	@ echo "Configuring kvm..."
	@ ( cd $(KVM_SRC_DIR); ./configure \
	--arch=i386 \
	--disable-bluez \
	--disable-sdl \
	--disable-gfx-check \
	--disable-vnc-tls \
	--disable-libfdt \
	--disable-nptl \
	--prefix=/usr )
	@ touch $@
# we may add --disable-vde 
	#--disable-aio \

$(KVM_SRC_DIR)/.built: $(KVM_STAMP_DIR)/.configured
	@ echo "Building kvm..."
	@ echo "LDFLAGS: $(LDFLAGS);"
	@ echo "LIBS: $(LIBS);"
	@ echo "LDFLAGS_orig: $(LDFLAGS_orig);"
	@ echo "CFLAGS: $(CFLAGS);"
	@ $(MAKE) -C $(KVM_SRC_DIR) VERBOSE=y \
	kcmd='#' prefix='/usr' LDFLAGS="$(LDFLAGS)" \
	LIBS="$(LIBS) -L$(KVM_SRC_DIR)/libkvm \
	-lrt -lz -lm -lutil -lpthread -lkvm -lgcc" \
        CFLAGS="-nostdlib $(CFLAGS) $(LIMITS_CFLAGS) -Wall \
	-I$(KVM_SRC_DIR)/libkvm -I$(KVM_SRC_DIR)/kernel/include \
	$(call cc-option,-muclibc,) \
	-D__USE_EXTERN_INLINES -DCONFIG_X86" all
	@ touch $@

$(INITRD_DIR)/bin/kvm: $(KVM_SRC_DIR)/.built
	@ echo "Installing kvm..."
	@ strip -s $(KVM_SRC_DIR)/qemu/qemu-img
	@ strip -s $(KVM_SRC_DIR)/qemu/x86_64-softmmu/kvm
	@ $(MAKE) DESTDIR=$(INITRD_DIR) kcmd='#' prefix='/usr'  \
	-C $(KVM_SRC_DIR) install
	@ rm -f $(INITRD_DIR)/usr/bin/qemu-nbd
	@ rm -rf $(INITRD_DIR)/usr/include
	@ rm -f $(INITRD_DIR)/usr/share/qemu/openbios-sparc*
	@ rm -f $(INITRD_DIR)/usr/share/qemu/ppc_rom.bin
	@ rm -f $(INITRD_DIR)/usr/share/qemu/pxe-*.bin

$(KVM_STAMP_DIR):
	mkdir -p $@

kvm: $(KVM_STAMP_DIR) $(INITRD_DIR)/bin/kvm

kvm-clean:
	@ rm -f $(KVM_STAMP_DIR)/.configured 
ifneq ($(wildcard $(KVM_SRC_DIR)/Makefile),)
	$(MAKE) -C $(KVM_SRC_DIR) clean
endif
kvm-distclean:
	rm -rf $(KVM_DIR)/*

