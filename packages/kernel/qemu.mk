# Build file for the QEMU LAB and AVATT kernel

KERNEL_URL=http://kernel.org/pub/linux/kernel/v2.6/
KERNEL_SOURCE=linux-$(KERNEL_VERSION).tar.bz2

# Kernel config is set in the platform configuration

TINY_URL=http://elinux.org/images/0/0e/
TINY_SOURCE=Tiny-quilt-2.6.22.1-1.tar.gz
TINY_DIR=$(KERNEL_DIR)/tiny/patches

KERNEL_PATCHES += $(TINY_DIR)
KERNEL_PATCHES += $(PACKAGE_DIR)/kernel/patches/kvm.patch
KERNEL_PATCHES += $(PACKAGE_DIR)/kernel/patches/gcc-4.3-fix.patch
KERNEL_PATCHES += $(PACKAGE_DIR)/kernel/patches/sigcontext.h.patch
KERNEL_PATCHES += $(PACKAGE_DIR)/kernel/patches/ext3.patch

$(SOURCE_DIR)/$(KERNEL_SOURCE):
	@ mkdir -p $(SOURCE_DIR)
	@ wget $(WGET_Q) -P $(SOURCE_DIR) $(KERNEL_URL)/$(KERNEL_SOURCE)

$(SOURCE_DIR)/$(TINY_SOURCE):
	@ mkdir -p $(SOURCE_DIR)
	@ wget $(WGET_Q) -P $(SOURCE_DIR) $(TINY_URL)/$(TINY_SOURCE)

include $(PACKAGE_DIR)/kernel/kernel.inc

kernel: generic-kernel
kernel-clean: generic-kernel-clean
kernel-distclean: generic-kernel-distclean
