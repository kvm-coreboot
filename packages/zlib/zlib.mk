ZLIB_URL=http://www.zlib.net/
ZLIB_VERSION=1.2.3
ZLIB_SOURCE=zlib-$(ZLIB_VERSION).tar.gz
ZLIB_DIR=$(BUILD_DIR)/zlib
ZLIB_SRC_DIR=$(ZLIB_DIR)/zlib-$(ZLIB_VERSION)
ZLIB_STAMP_DIR=$(ZLIB_DIR)/stamps
ZLIB_LOG_DIR=$(ZLIB_DIR)/logs

ZLIB_PATCHES=$(PACKAGE_DIR)/zlib/patches

ifeq ($(CONFIG_VERBOSE),y)
ZLIB_BUILD_LOG=/dev/stdout
ZLIB_CONFIG_LOG=/dev/stdout
else
ZLIB_BUILD_LOG=$(ZLIB_LOG_DIR)/build.log
ZLIB_CONFIG_LOG=$(ZLIB_LOG_DIR)/config.log
endif

CC_IPREFIX:=$(shell $(CC) --print-file-name=include)
LIMITS_CFLAGS = -I$(dir $(CC_IPREFIX))/include-fixed -I$(CC_IPREFIX)

$(SOURCE_DIR)/$(ZLIB_SOURCE):
	@ echo "Downloading zlib..."
	@ mkdir -p $(SOURCE_DIR)
	@ wget $(WGET_Q) -P $(SOURCE_DIR) $(ZLIB_URL)/$(ZLIB_SOURCE)

$(ZLIB_STAMP_DIR)/.unpacked: $(SOURCE_DIR)/$(ZLIB_SOURCE)
	@ echo "Unpacking zlib..."
	@ tar -C $(ZLIB_DIR) -zxf $(SOURCE_DIR)/$(ZLIB_SOURCE)
	@ touch $@

$(ZLIB_STAMP_DIR)/.patched: $(ZLIB_STAMP_DIR)/.unpacked
	@ $(BIN_DIR)/doquilt.sh $(ZLIB_SRC_DIR) $(ZLIB_PATCHES)
	@ sed -i -e '/ldconfig/d' $(ZLIB_SRC_DIR)/Makefile*
	@ touch $@

$(ZLIB_STAMP_DIR)/.configured: $(ZLIB_STAMP_DIR)/.patched
	@ (cd $(ZLIB_SRC_DIR); ./configure --prefix=$(STAGING_DIR) \
	--libdir=$(STAGING_DIR)/lib --shared)
	@ touch $@

$(ZLIB_SRC_DIR): $(ZLIB_STAMP_DIR)/.configured
	@ echo "Building zlib..."
	@ $(MAKE) CFLAGS="$(CROSS_CFLAGS) $(LIMITS_CFLAGS) -Os \
	-I$(STAGING_DIR)/include \
	$(call cc-option,-fno-stack-protector,) \
	$(call cc-option,-fno-stack-protector-all,) " \
	LDFLAGS+="$(LIBS)" \
	-C $(ZLIB_SRC_DIR) libz.so.$(ZLIB_VERSION) libz.a 

$(STAGING_DIR)/include/zlib.h: $(ZLIB_SRC_DIR)
	@ install $(ZLIB_SRC_DIR)/zlib.h $(STAGING_DIR)/include -m 0644
	@ install $(ZLIB_SRC_DIR)/zconf.h $(STAGING_DIR)/include -m 0644
	@ install $(ZLIB_SRC_DIR)/libz.a $(STAGING_DIR)/lib -m 0644
	@ install $(ZLIB_SRC_DIR)/libz.so.$(ZLIB_VERSION) $(STAGING_DIR)/lib
	@ (cd $(STAGING_DIR)/lib ; ln -sf libz.so.$(ZLIB_VERSION) libz.so)
	@ (cd $(STAGING_DIR)/lib ; ln -sf libz.so.$(ZLIB_VERSION) libz.so.1)
	@ strip -s $(STAGING_DIR)/lib/libz.so.$(ZLIB_VERSION)

$(ZLIB_STAMP_DIR) $(ZLIB_LOG_DIR):
	@ mkdir -p $@

zlib: $(ZLIB_STAMP_DIR) $(ZLIB_LOG_DIR) $(STAGING_DIR)/include/zlib.h

zlib-clean:
	@ echo "Cleaning zlib..."
ifneq ($(wildcard $(ZLIB_SRC_DIR)/Makefile),)
	@ $(MAKE) -C $(ZLIB_SRC_DIR) clean > /dev/null 2>&1
endif

zlib-distclean:
	@ rm -rf $(ZLIB_DIR)/*

zlib-extract: $(ZLIB_STAMP_DIR)/.unpacked

