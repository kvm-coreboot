NCURSES_URL=http://ftp.gnu.org/pub/gnu/ncurses/
NCURSES_VERSION=5.6
NCURSES_SOURCE=ncurses-$(NCURSES_VERSION).tar.gz
NCURSES_DIR=$(BUILD_DIR)/ncurses
NCURSES_SRC_DIR=$(NCURSES_DIR)/ncurses-$(NCURSES_VERSION)
NCURSES_STAMP_DIR=$(NCURSES_DIR)/stamps
NCURSES_LOG_DIR=$(NCURSES_DIR)/logs

ifeq ($(CONFIG_VERBOSE),y)
NCURSES_BUILD_LOG=/dev/stdout
NCURSES_CONFIG_LOG=/dev/stdout
else
NCURSES_BUILD_LOG=$(NCURSES_LOG_DIR)/build.log
NCURSES_CONFIG_LOG=$(NCURSES_LOG_DIR)/config.log
endif

CC_IPREFIX:=$(shell $(CC) --print-file-name=include)
LIMITS_CFLAGS = -I$(dir $(CC_IPREFIX))/include-fixed -I$(CC_IPREFIX)

$(SOURCE_DIR)/$(NCURSES_SOURCE):
	@ echo "Downloading ncurses..."
	@ mkdir -p $(SOURCE_DIR)
	@ wget $(WGET_Q) -P $(SOURCE_DIR) $(NCURSES_URL)/$(NCURSES_SOURCE)

$(NCURSES_STAMP_DIR)/.patched: $(SOURCE_DIR)/$(NCURSES_SOURCE)
	@ echo "Unpacking ncurses..."
	@ tar -C $(NCURSES_DIR) -zxf $(SOURCE_DIR)/$(NCURSES_SOURCE)
	@ #sed -e 's~\$$srcdir/shlib tic\$$suffix~/usr/bin/tic~'	$(NCURSES_SRC_DIR)/misc/run_tic.in
	@ cp -f $(PACKAGE_DIR)/ncurses/patches/config.* $(NCURSES_SRC_DIR)/
	@ touch $@

$(NCURSES_STAMP_DIR)/.configured: $(NCURSES_STAMP_DIR)/.patched
	(cd $(NCURSES_SRC_DIR); rm -rf config.cache; \
	 ./configure \
	 --with-build-cc=`which gcc` \
	 --target=i686-pc-linux-uclibc \
	 --host=i686-pc-linux-gnu \
	 --build=i686-pc-linux-uclibc \
	 --prefix=/usr \
	 --exec-prefix=/usr \
	 --bindir=/usr/bin \
	 --sbindir=/usr/sbin \
	 --libdir=/lib \
	 --libexecdir=/usr/lib \
	 --sysconfdir=/etc \
	 --datadir=/usr/share \
	 --localstatedir=/var \
	 --includedir=/usr/include \
	 --mandir=/usr/man \
	 --infodir=/usr/info \
	 --with-terminfo-dirs=/usr/share/terminfo \
	 --with-default-terminfo-dir=/usr/share/terminfo \
	 --with-shared --without-cxx --without-cxx-binding \
	 --without-ada --without-progs --disable-big-core \
	 --without-profile --without-debug --disable-rpath \
	 --enable-echo --enable-const --enable-overwrite \
	 --enable-broken_linker \
	 --with-shared \
	 --without-gpm )
	 @ touch $@

$(NCURSES_SRC_DIR)/lib/libncurses.so.$(NCURSES_VERSION): $(NCURSES_STAMP_DIR)/.configured
	@ $(MAKE) DESTDIR=$(STAGING_DIR) CFLAGS+="$(LIMITS_CFLAGS)" -C $(NCURSES_SRC_DIR) \
		libs panel menu form headers

$(STAGING_DIR)/lib/libncurses.so.$(NCURSES_VERSION): $(NCURSES_SRC_DIR)/lib/libncurses.so.$(NCURSES_VERSION)
	@ mkdir -p $(STAGING_DIR)/junk $(STAGING_DIR)/usr/share 
	@ $(MAKE) \
	prefix=$(STAGING_DIR)/usr/local \
	exec_prefix=$(STAGING_DIR)/junk \
	bindir=$(STAGING_DIR)/junk \
	sbindir=$(STAGING_DIR)/junk \
	libexecdir=$(STAGING_DIR)/junk \
	datadir=$(STAGING_DIR)/usr/share \
	sysconfdir=$(STAGING_DIR)/etc \
	localstatedir=$(STAGING_DIR)/junk \
	libdir=$(STAGING_DIR)/lib \
	infodir=$(STAGING_DIR)/junk \
	mandir=$(STAGING_DIR)/junk \
	includedir=$(STAGING_DIR)/include \
	gxx_include_dir=$(STAGING_DIR)/junk \
	ticdir=$(STAGING_DIR)/usr/share/terminfo \
	-C $(NCURSES_SRC_DIR) install
	@ touch -c $@

$(NCURSES_STAMP_DIR) $(NCURSES_LOG_DIR):
	@ mkdir -p $@

$(INITRD_DIR)/usr/share/terminfo/l:
	@ mkdir -p $@

$(INITRD_DIR)/usr/share/terminfo/l/linux: $(STAGING_DIR)/lib/libncurses.so.$(NCURSES_VERSION) $(INITRD_DIR)/usr/share/terminfo/l
	@ cp $(STAGING_DIR)/usr/share/terminfo/l/linux $@

$(INITRD_DIR)/usr/share/tabset: $(STAGING_DIR)/lib/libncurses.so.$(NCURSES_VERSION) $(INITRD_DIR)/usr/share/terminfo/l
	@ cp -r $(STAGING_DIR)/usr/share/tabset $@

ncurses: $(NCURSES_STAMP_DIR) $(NCURSES_LOG_DIR) $(STAGING_DIR)/lib/libncurses.so.$(NCURSES_VERSION) $(INITRD_DIR)/usr/share/terminfo/l/linux $(INITRD_DIR)/usr/share/tabset

ncurses-clean:
	@ echo "Cleaning ncurses..."
ifneq ($(wildcard $(NCURSES_SRC_DIR)/Makefile),)
	@ $(MAKE) -C $(NCURSES_SRC_DIR) clean > /dev/null 2>&1
endif

ncurses-distclean:
	@ rm -rf $(NCURSES_DIR)/*

ncurses-extract: $(NCURSES_STAMP_DIR)/.unpacked

